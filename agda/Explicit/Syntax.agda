{-# OPTIONS --without-K --safe #-}

module Explicit.Syntax where

open import Default

open import Relation.Nullary

infixr 10 _⇒_

data Typ : Set where
  B   : Typ
  _⇒_ : Typ → Typ → Typ
  □   : Typ → Typ

infixr 0 _$_

data Trm : Set where
  v      : ℕ → Trm                   -- local variable
  Λ      : Trm → Trm
  _$_    : Trm → Trm → Trm
  g      : ℕ → Trm                   -- global variable
  box    : Trm → Trm
  letbox : Trm → Trm → Trm

Ctx : Set
Ctx = List Typ

variable
  S T U    : Typ
  S′ T′ U′ : Typ
  s t u    : Trm
  Γ Γ′     : Ctx
  Δ Δ′     : Ctx
  x y z    : ℕ
  m n      : ℕ

infixl 7 _↑v_ _↑v[_]_ _!v _↓v_
_↑v[_]_ : Trm → ℕ → ℕ → Trm
v x ↑v[ k ] n with n ≤? x
... | yes p          = v (k + x)
... | no ¬p          = v x
Λ t ↑v[ k ] n        = Λ (t ↑v[ k ] suc n)
(s $ u) ↑v[ k ] n    = s ↑v[ k ] n $ u ↑v[ k ] n
g x ↑v[ _ ] n        = g x
box t ↑v[ _ ] n      = box t
letbox s u ↑v[ k ] n = letbox (s ↑v[ k ] n) (u ↑v[ k ] n)

_↑v_ : Trm → ℕ → Trm
t ↑v n = t ↑v[ 1 ] n

_!v : Trm → Trm
_!v = _↑v 0

_↓v_ : Trm → ℕ → Trm
v x ↓v n with n <? x
... | yes (s≤s {n = m} p) = v m
... | no _                = v x
Λ t ↓v n                  = Λ (t ↓v suc n)
(s $ u) ↓v n              = s ↓v n $ u ↓v n
g x ↓v n                  = g x
box t ↓v n                = box t
letbox t u ↓v n           = letbox (t ↓v n) (u ↓v n)

infixl 7 _↑g_ _↑g[_]_ _!g
_↑g[_]_ : Trm → ℕ → ℕ → Trm
v x ↑g[ _ ] n        = v x
Λ t ↑g[ k ] n        = Λ (t ↑g[ k ] n)
(s $ u) ↑g[ k ] n    = s ↑g[ k ] n $ u ↑g[ k ] n
g x ↑g[ k ] n with n ≤? x
... | yes _     = g (k + x)
... | no _      = g x
box t ↑g[ k ] n      = box (t ↑g[ k ] n)
letbox s u ↑g[ k ] n = letbox (s ↑g[ k ] n) (u ↑g[ k ] suc n)

_↑g_ : Trm → ℕ → Trm
t ↑g n = t ↑g[ 1 ] n

_!g : Trm → Trm
_!g = _↑g 0

infixl 5 _[_/_]v _[_/_]g

_[_/_]v : Trm → Trm → ℕ → Trm
v x [ s / n ]v with x ≟ n
... | yes _           = s
... | no _            = v x
Λ t [ s / n ]v        = Λ (t [ s ↑v 0 / suc n ]v)
(t $ u) [ s / n ]v    = t [ s / n ]v $ u [ s / n ]v
g x [ s / n ]v        = g x
box t [ s / n ]v      = box t
letbox t u [ s / n ]v = letbox (t [ s / n ]v) (u [ s / n ]v)

_[_/_]g : Trm → Trm → ℕ → Trm
v x [ s / n ]g        = v x
Λ t [ s / n ]g        = Λ (t [ s / n ]g)
(t $ u) [ s / n ]g    = t [ s / n ]g $ u [ s / n ]g
g x [ s / n ]g with x ≟ n
... | yes _           = s
... | no _            = g x
box t [ s / n ]g      = box (t [ s / n ]g)
letbox t u [ s / n ]g = letbox (t [ s / n ]g) (u [ s ↑g 0 / suc n ]g)

infix 4 _↦_∈_
data _↦_∈_ : ℕ → Typ → Ctx → Set where
  hd : 0 ↦ T ∈ T ∷ Γ
  tl : n ↦ T ∈ Γ → suc n ↦ T ∈ T′ ∷ Γ

infix -10 _∥_⊢_∶_
data _∥_⊢_∶_ : Ctx → Ctx → Trm → Typ → Set where
  vlkup : x ↦ T ∈ Γ →
          Δ ∥ Γ ⊢ v x ∶ T
  glkup : x ↦ T ∈ Δ →
          Δ ∥ Γ ⊢ g x ∶ T
  ⇒-i   : Δ ∥ S ∷ Γ ⊢ t ∶ T →
          Δ ∥ Γ ⊢ Λ t ∶ S ⇒ T
  ⇒-e   : Δ ∥ Γ ⊢ t ∶ S ⇒ U →
          Δ ∥ Γ ⊢ s ∶ S →
          Δ ∥ Γ ⊢ t $ s ∶ U
  □-i   : Δ ∥ [] ⊢ t ∶ T →
          Δ ∥ Γ ⊢ box t ∶ □ T
  □-e   : Δ ∥ Γ ⊢ t ∶ □ T →
          T ∷ Δ ∥ Γ ⊢ u ∶ U →
          Δ ∥ Γ ⊢ letbox t u ∶ U
