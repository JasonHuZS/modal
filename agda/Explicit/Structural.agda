{-# OPTIONS --without-K --safe #-}

module Explicit.Structural where

open import Default
open import Explicit.Syntax

open import Relation.Nullary

∈-weaken-≤ : ∀ Γ₁ Γ₂ → x ↦ T ∈ Γ →
               Γ ≡ Γ₁ ++ Γ₂ →
               length Γ₁ ≤ x →
               suc x ↦ T ∈ Γ₁ ++ S ∷ Γ₂
∈-weaken-≤ [] _ hd refl ≤                  = tl hd
∈-weaken-≤ [] _ (tl D) refl ≤              = tl (tl D)
∈-weaken-≤ (S ∷ Γ₁) Γ₂ (tl D) refl (s≤s ≤) = tl (∈-weaken-≤ _ _ D refl ≤)

∈-weaken-> : ∀ Γ₁ Γ₂ → x ↦ T ∈ Γ →
               Γ ≡ Γ₁ ++ Γ₂ →
               ¬ (length Γ₁ ≤ x) →
               x ↦ T ∈ Γ₁ ++ S ∷ Γ₂
∈-weaken-> [] Γ₂ hd eq >             = ⊥-elim (> z≤n)
∈-weaken-> [] Γ₂ (tl D) eq >         = ⊥-elim (> z≤n)
∈-weaken-> (U ∷ Γ₁) Γ₂ hd refl >     = hd
∈-weaken-> (U ∷ Γ₁) Γ₂ (tl D) refl > = tl (∈-weaken-> _ _ D refl (λ >′ → > (s≤s >′)))

local-weakening-gen : ∀ {Γ₁ Γ₂} → Δ ∥ Γ ⊢ t ∶ T →
                        Γ ≡ Γ₁ ++ Γ₂ →
                        Δ ∥ Γ₁ ++ S ∷ Γ₂ ⊢ t ↑v length Γ₁ ∶ T
local-weakening-gen {Γ₁ = Γ₁} (vlkup {x = x} T∈Γ) eq with length Γ₁ ≤? x
... | yes l≤x                  = vlkup (∈-weaken-≤ _ _ T∈Γ eq l≤x)
... | no l>x                   = vlkup (∈-weaken-> _ _ T∈Γ eq l>x)
local-weakening-gen (glkup T∈Δ) eq = glkup T∈Δ
local-weakening-gen (⇒-i D) eq     = ⇒-i (local-weakening-gen D (≡.cong (_ ∷_) eq))
local-weakening-gen (⇒-e D D′) eq  = ⇒-e (local-weakening-gen D eq) (local-weakening-gen D′ eq)
local-weakening-gen (□-i D) eq     = □-i D
local-weakening-gen (□-e D D′) eq  = □-e (local-weakening-gen D eq) (local-weakening-gen D′ eq)

local-weakening : ∀ Γ₁ {Γ₂} → Δ ∥ Γ₁ ++ Γ₂ ⊢ t ∶ T → Δ ∥ Γ₁ ++ S ∷ Γ₂ ⊢ t ↑v length Γ₁ ∶ T
local-weakening _ D = local-weakening-gen D refl

_‼v_ : Trm → Ctx → Trm
t ‼v Γ = repeat _!v (length Γ) t

local-weakening* : ∀ Γ₁ {Γ₂} → Δ ∥ Γ₂ ⊢ t ∶ T → Δ ∥ Γ₁ ++ Γ₂ ⊢ t ‼v Γ₁ ∶ T
local-weakening* [] D       = D
local-weakening* (S ∷ Γ₁) D = local-weakening [] (local-weakening* Γ₁ D)

global-weakening-gen : ∀ {Δ₁ Δ₂} → Δ ∥ Γ ⊢ t ∶ T →
                         Δ ≡ Δ₁ ++ Δ₂ →
                         Δ₁ ++ S ∷ Δ₂ ∥ Γ ⊢ t ↑g length Δ₁ ∶ T
global-weakening-gen (vlkup T∈Γ) eq = vlkup T∈Γ
global-weakening-gen {Δ₁ = Δ₁} (glkup {x = x} T∈Δ) eq with length Δ₁ ≤? x
... | yes l≤x                   = glkup (∈-weaken-≤ _ _ T∈Δ eq l≤x)
... | no l>x                    = glkup (∈-weaken-> _ _ T∈Δ eq l>x)
global-weakening-gen (⇒-i D) eq     = ⇒-i (global-weakening-gen D eq)
global-weakening-gen (⇒-e D D′) eq  = ⇒-e (global-weakening-gen D eq) (global-weakening-gen D′ eq)
global-weakening-gen (□-i D) eq     = □-i (global-weakening-gen D eq)
global-weakening-gen (□-e D D′) eq  = □-e (global-weakening-gen D eq) (global-weakening-gen D′ (≡.cong (_ ∷_) eq))

global-weakening : ∀ Δ₁ {Δ₂} → Δ₁ ++ Δ₂ ∥ Γ ⊢ t ∶ T → Δ₁ ++ S ∷ Δ₂ ∥ Γ ⊢ t ↑g length Δ₁ ∶ T
global-weakening _ D = global-weakening-gen D refl

local-subst : ∀ {Γ₁ Γ₂} → Δ ∥ Γ₂ ⊢ s ∶ S →
              Δ ∥ Γ′ ⊢ u ∶ U → Γ′ ≡ Γ₁ ++ S ∷ Γ₂ →
              Δ ∥ Γ₁ ++ Γ₂ ⊢ u ↓v length Γ₁ [ s ‼v Γ₁ / length Γ₁ ]v ∶ U
local-subst {Γ₁ = Γ₁} D (vlkup {x = x} U∈Γ′) eq with compare (length Γ₁) x
... | less x>l = {!!}
... | equal with length Γ₁ ≟ length Γ₁
... | yes _ = local-weakening* Γ₁ {!D!}
... | no _ = {!res!}
local-subst {Γ₁ = Γ₁} D (vlkup {x = x} U∈Γ′) eq | greater x<l = {!!}
local-subst D (glkup U∈Δ) eq = glkup U∈Δ
local-subst D (⇒-i D′) eq = {!!}
local-subst D (⇒-e D′ D″) eq = ⇒-e (local-subst D D′ eq) (local-subst D D″ eq)
local-subst D (□-i D′) eq = {!!}
local-subst D (□-e D′ D″) eq = {!!}
