{-# OPTIONS --without-K --safe #-}

module Default where

open import Data.Nat as ℕ using (ℕ; zero; suc; _+_; _≤_; _<_; _>_; _≥_; s≤s; z≤n) public
open import Data.Empty public
open import Data.List public
open import Relation.Nullary using (yes; no) public
open import Relation.Binary

open import Utils public

import Data.Nat.Properties as ℕₚ

import Relation.Binary.PropositionalEquality

module ≡ = Relation.Binary.PropositionalEquality
open ≡ using (_≡_; refl) public

data CmpResult (a : ℕ) : ℕ → Set where
  less : ∀ {b} → a < b → CmpResult a b
  equal : CmpResult a a
  greater : ∀ {b} → a > b → CmpResult a b

compare : ∀ a b → CmpResult a b
compare zero zero    = equal
compare zero (suc b) = less (s≤s z≤n)
compare (suc a) zero = greater (s≤s z≤n)
compare (suc a) (suc b) with compare a b
... | less a<b       = less (s≤s a<b)
... | equal          = equal
... | greater a>b    = greater (s≤s a>b)

_≟_ : Decidable {A = ℕ} _≡_
a ≟ b with compare a b
... | less a<b = no (ℕₚ.<⇒≢ a<b)
... | equal = yes refl
... | greater a>b = no (≡.≢-sym (ℕₚ.<⇒≢ a>b))

_≤?_ : Decidable {A = ℕ} _≤_
a ≤? b with compare a b
... | less a<b      = yes (ℕₚ.<⇒≤ a<b)
... | equal         = yes ℕₚ.≤-refl
... | greater a>b   = no (ℕₚ.<⇒≱ a>b)

_<?_ : Decidable {A = ℕ} _<_
a <? b with compare a b
... | less a<b      = yes a<b
... | equal         = no (ℕₚ.<-irrefl refl)
... | greater a>b   = no (ℕₚ.<⇒≯ a>b)
