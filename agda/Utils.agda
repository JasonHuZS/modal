{-# OPTIONS --without-K --safe #-}

module Utils where

open import Data.Nat

repeat : ∀ {a} {A : Set a} → (A → A) → ℕ → A → A
repeat f zero x    = x
repeat f (suc n) x = f (repeat f n x)
